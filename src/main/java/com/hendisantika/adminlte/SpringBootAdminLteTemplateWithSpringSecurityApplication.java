package com.hendisantika.adminlte;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 8/9/17
 * Time: 7:07 PM
 * To change this template use File | Settings | File Templates.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAdminLteTemplateWithSpringSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAdminLteTemplateWithSpringSecurityApplication.class, args);
	}
}
