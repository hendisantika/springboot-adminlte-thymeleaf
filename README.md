# springboot-adminlte-thymeleaf

SpringBoot AdminLTE Thymeleaf template with Spring Security

Run this project by this command : `mvn clean spring-boot:run`

### Screenshot

Index Page

![Index Page](img/index.png "Index Page")

404 Page

![404 Page](img/404.png "404 Page")

500 Page

![500 Page](img/500.png "500 Page")

